spam = []
ham = []
 
sms_list = open("SMSSpamCollection.txt")

for line in sms_list:
    line = line.rstrip()
    info = line.split("\t")
    if (info[0] == "ham"):
        ham.append(info[1])
    elif (info[0] == "spam"):
        spam.append(info[1])

sms_list.close()

def average_word_count(sms_list):
    total_words = 0
    for sms in sms_list:
        total_words += len(sms.split(" "))
    return total_words / len(sms_list)

def ends_with_exclamation_mark(sms_list):
    ex_counter = 0  
    for sms in sms_list:
        if sms[-1] == '!':
            ex_counter += 1
    return ex_counter
 
print(f"Prosjecan broj rijeci u spamu {average_word_count(spam)}")
print(f"Prosjecan broj rijeci u hamu {average_word_count(ham)}")
print(f"Broj poruka tipa spam sa ! na kraju: {ends_with_exclamation_mark(spam)}")