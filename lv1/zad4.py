word_count = {}

song = open('song.txt')

for line in song:
    line = line.rstrip()
    words = line.split(" ")
    for word in words:
        if word not in word_count:
            word_count[word] = 1
        else:
            word_count[word] += 1

song.close()

unique_words = 0

for word in word_count:
    if word_count[word] == 1:
        unique_words += 1
        print(f'{word}')

print(f'Broj jedinstvenih rijeci je: {unique_words}')