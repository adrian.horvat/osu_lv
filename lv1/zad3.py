brojevi = []

while(True):
    unos = input('Unesi broj: ')
    if unos == 'Done':
        break;
    else:
        try:
            brojevi.append(float(unos))
        except:
            print('Krivi unos broja')

brojevi.sort()

print(f'Korisnik je unjeo {len(brojevi)} brojeva u listu')
print(f'Srednja vrijednost liste brojeva iznosi: {sum(brojevi)/len(brojevi)}')
print(f'Minimalna vrijednost liste je {min(brojevi)}')
print(f'Maksimalna vrijednost liste je {max(brojevi)}')
print(f'Sortirana lista {brojevi}')