import pandas as pd
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
import sklearn.linear_model as lm
from sklearn.metrics import mean_absolute_error, mean_squared_error, mean_absolute_percentage_error, r2_score

data = pd.read_csv('data_C02_emission.csv')
data = data.drop(['Make','Model'], axis = 1)

#a
input_variables = ['Engine Size (L)','Cylinders','Fuel Consumption City (L/100km)','Fuel Consumption Hwy (L/100km)','Fuel Consumption Comb (L/100km)','Fuel Consumption Comb (mpg)']
output = 'CO2 Emissions (g/km)'

X = data[input_variables]
y = data[output]

X_train , X_test , y_train , y_test = train_test_split (X, y, test_size = 0.2, random_state =1)

#b
plt.figure()
plt.scatter(X_train['Fuel Consumption Comb (L/100km)'],y_train,c='red')
plt.scatter(X_test['Fuel Consumption Comb (L/100km)'],y_test,c='blue')
plt.show()

#c
plt.hist(X_train['Fuel Consumption Comb (L/100km)'])
plt.show()

sc = MinMaxScaler ()
X_train_n = sc.fit_transform(X_train)

plt.hist(X_train_n[:, 0],bins=20)
plt.show()

X_test_n = sc.transform(X_test)

#d
linearModel = lm.LinearRegression()
linearModel.fit(X_train_n, y_train)
print(linearModel.coef_)

#e
y_test_p = linearModel.predict(X_test_n)

plt.figure()
plt.scatter(X_test_n[:,4],y_test,c='red')
plt.scatter(X_test_n[:,4],y_test_p,c='blue')
plt.show()

#f

MSE = mean_squared_error(y_test , y_test_p)
RMSE = mean_squared_error(y_test, y_test_p, squared=False)
MAE = mean_absolute_error(y_test , y_test_p)
MAPE = mean_absolute_percentage_error(y_test, y_test_p)
R2_SCORE = r2_score(y_test, y_test_p)
print(f"MSE: {MSE}, RMSE: {RMSE}, MAE: {MAE}, MAPE: {MAPE}, R2 SCORE: {R2_SCORE}")