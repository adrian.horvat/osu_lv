import numpy as np
from tensorflow import keras
from PIL import Image

model = keras.models.load_model('FCN/zad_1_model.h5')

for i in range(10):
    image_path = f'test_{i}.png'
    image = Image.open(image_path).convert('L') 
    image = image.resize((28, 28))  
    image_array = np.array(image)  

    image_array = image_array.astype("float32") / 255
    image_array = np.expand_dims(image_array, axis=0)
    image_array = np.expand_dims(image_array, axis=-1)

    predicted_label = np.argmax(model.predict(image_array))

    print(f"Predicted label for {i}: {predicted_label}")

'''
Predicted label for 0: 2

Predicted label for 1: 4

Predicted label for 2: 7

Predicted label for 3: 7

Predicted label for 4: 8

Predicted label for 5: 5

Predicted label for 6: 5

Predicted label for 7: 7

Predicted label for 8: 4

Predicted label for 9: 9
'''