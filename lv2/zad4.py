import numpy as np
import matplotlib.pyplot as plt

zeros = np.zeros((50, 50))
ones = np.ones((50, 50))

top = np.hstack((zeros, ones))
bot = np.hstack((ones, zeros))
slika = np.vstack((top, bot))

plt.figure()
plt.imshow(slika, cmap="gray")
plt.show()