import numpy as np
import matplotlib . pyplot as plt

img = plt.imread("road.jpg")

plt.figure()
plt.imshow(img, alpha=0.5)
plt.show()

sirina_slike = len(img[0])
quarter_width = int(sirina_slike/4)
plt.imshow(img[:, quarter_width: 2*quarter_width , :])
plt.show()

rotirana = np.rot90(img, 3)
plt.imshow(rotirana)
plt.show()

zrcaljena = np.flip(img, 1)
plt.imshow(zrcaljena)
plt.show()