import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt('data.csv', delimiter=',', dtype=str)

#a
data = (data[1::])
print(f'Broj ljudi: {len(data)}')

#b
data = np.array(data, np.float64)
height = data[:, 1]
weight = data[:, 2]
plt.scatter(height, weight)
plt.show()

#c
height_50 = data[0::50, 1]
weight_50 = data[0::50, 2]
plt.scatter(height_50, weight_50)
plt.show()

#d
print(f'Prosjecna visina: {height.mean()}')
print(f'Max visina: {height.max()}')
print(f'Min visina: {height.min()}')

#e
zene =  data[data[:,0] == 0]
muskaraci =  data[data[:,0] == 1]
print(f'Muskarci -- Max visina: {muskaraci[:, 1].max()}, min visina: {muskaraci[:, 1].min()}, prosjecna visina: {muskaraci[:, 1].mean()}')
print(f'Zene -- Max visina: {zene[:, 1].max()}, min visina: {zene[:, 1].min()}, prosjecna visina: {zene[:, 1].mean()}')


