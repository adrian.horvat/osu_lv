import pandas as pd

data = pd.read_csv('data_C02_emission.csv')

largest = data['Fuel Consumption City (L/100km)'].nlargest(3).index
smallest = data['Fuel Consumption City (L/100km)'].nsmallest(3).index

print(data.loc[largest, ['Make', 'Model', 'Fuel Consumption City (L/100km)']])
print(data.loc[smallest, ['Make', 'Model', 'Fuel Consumption City (L/100km)']])