import pandas as pd

data = pd.read_csv('data_C02_emission.csv')

transmissons = data['Transmission']

manual_transmissions = sum(1 for type in transmissons if 'M' in type)

print('Broj auta s rucnim mjenjacem je:',manual_transmissions)