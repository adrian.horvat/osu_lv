import pandas as pd
import matplotlib.pyplot as plt

data = pd.read_csv('data_C02_emission.csv')

fuel_type_colors = {'X': 'red', 'Z': 'blue', 'D': 'gray', 'E': 'green','N': 'yellow'}
colors = data['Fuel Type'].map(fuel_type_colors)

data.plot.scatter(x='Fuel Consumption City (L/100km)',y='CO2 Emissions (g/km)',c=colors,s=50)

plt.show()

'''
Ovaj vizualni prikaz omogućuje lako prepoznavanje
i usporedbu odnosa između ovih veličina za različite tipove goriva.
Na primjer, elektricna vozila se nalaze ispod ostalih boja u grafu, sto znaci da manje zagađuju.
'''