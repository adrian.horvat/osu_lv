import pandas as pd

data = pd.read_csv('data_C02_emission.csv')

print("Broj redaka:",len(data))

print("Tipovi podataka:")
print(data.dtypes)

print("Izostale vrijednosti:")
print(data.isnull().sum())

duplicates = data.duplicated().sum()
print("Broj dupliciranih vrijednosti:", duplicates)

data = data.drop_duplicates()

category_convert = data.select_dtypes(include=['object'])
for column in category_convert.columns:
    data[column]=data[column].astype('category')

print(category_convert.dtypes)