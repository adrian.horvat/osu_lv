import pandas as pd
import matplotlib.pyplot as plt

data = pd.read_csv('data_C02_emission.csv')

avg_co2_by_cylinders = data.groupby('Cylinders')['CO2 Emissions (g/km)'].mean()

avg_co2_by_cylinders.plot(kind='bar')

plt.title('Average CO2 Emissions by Cylinders')
plt.xlabel('Cylinders')
plt.ylabel('CO2 Emissions (g/km)')
plt.xticks(rotation=0)
plt.show()