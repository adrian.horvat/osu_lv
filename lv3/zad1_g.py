import pandas as pd

data = pd.read_csv('data_C02_emission.csv')

filtered = data[(data['Cylinders']==4) & (data['Fuel Type']=='D')]

print('Najveca prosječna potrosnja goriva za 4 cilindra i dizel je:\n',filtered.nlargest(1,'Fuel Consumption City (L/100km)'))