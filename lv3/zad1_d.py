import pandas as pd

data = pd.read_csv('data_C02_emission.csv')

audi_cars = data[data['Make']=='Audi']
audi_4_cylinders_average_emission = audi_cars[audi_cars['Cylinders']==4]['CO2 Emissions (g/km)'].mean()

print("Broj Audi automobila:",len(audi_cars))
print("Prosjecna CO2 emisija Audi automobila s 4 cilindra:",audi_4_cylinders_average_emission)