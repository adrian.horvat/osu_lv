import pandas as pd

data = pd.read_csv('data_C02_emission.csv')

even_cylinders = data[(data['Cylinders'] % 2 == 0)]

average_emission_by_cylinders = even_cylinders.groupby('Cylinders')['CO2 Emissions (g/km)'].mean()

print("Broj vozila po broju cilindara:")
print(len(even_cylinders))
print("\nProsječna emisija CO2 po broju cilindara:")
print(average_emission_by_cylinders)