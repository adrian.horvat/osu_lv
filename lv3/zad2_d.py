import pandas as pd
import matplotlib.pyplot as plt

data = pd.read_csv('data_C02_emission.csv')

fuel_count = data['Fuel Type'].value_counts()
fuel_count.plot(kind='bar')

plt.xlabel('Fuel type')
plt.ylabel('Number of vehicles')
plt.xticks(rotation=0)
plt.title('Number of vehicles per fuel type')
plt.show()