import pandas as pd

data = pd.read_csv('data_C02_emission.csv')

filtered_cars = data[(data['Engine Size (L)']>=2.5) & (data['Engine Size (L)']<=3.5)]

print("Broj vozila koja imaju velicinu motora izmedu 2.5L i 3.5L:",len(filtered_cars))
print("Prosjecna CO2 emisija tih vozila:",filtered_cars['CO2 Emissions (g/km)'].mean())