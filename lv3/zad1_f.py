import pandas as pd

data = pd.read_csv('data_C02_emission.csv')

diesel_avg_consumption = data[data['Fuel Type']=='D']['Fuel Consumption City (L/100km)'].mean()
petrol_avg_consumption = data[data['Fuel Type']=='X']['Fuel Consumption City (L/100km)'].mean()

print('Prosječna potrosnja goriva za dizel je: ',diesel_avg_consumption)
print('Prosječna potrosnja goriva za benzin je: ',petrol_avg_consumption)

print('Medijalna vrijednost za diesel je: ',data[data['Fuel Type']=='D']['Fuel Consumption City (L/100km)'].median())
print('Medijalna vrijednost za benzin je: ',data[data['Fuel Type']=='X']['Fuel Consumption City (L/100km)'].median())