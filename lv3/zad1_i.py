import pandas as pd

data = pd.read_csv('data_C02_emission.csv')

print(data.corr(numeric_only=True))

'''
Engine Size (L) i Cylinders imaju visoku pozitivnu korelaciju od 0.920905. Ovo je očekivano jer veći motori često imaju više cilindara.

Fuel Consumption City, Fuel Consumption Hwy, Fuel Consumption Comb i CO2 Emissions također imaju visoke korelacije međusobno, što je i očekivano s obzirom na to da su svi ti parametri povezani s potrošnjom goriva i emisijom CO2.

Fuel Consumption Comb (mpg) pokazuje visoku negativnu korelaciju s ostalim parametrima, što je i očekivano jer se radi o suprotnoj metričkoj jedinici potrošnje goriva u odnosu na ostale parametre.
'''